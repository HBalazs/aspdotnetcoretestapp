﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace ASPNETCoreWebAppTest2.Models
{
    [Table("test")]
    public class Test
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}
