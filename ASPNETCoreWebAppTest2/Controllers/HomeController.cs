﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ASPNETCoreWebAppTest2.Models;
using Microsoft.Extensions.Logging;
using Serilog;
using System.Net.Mail;
using System.Net;

namespace ASPNETCoreWebAppTest2.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext applicationDbContext;

        //Constructor
        public HomeController(ApplicationDbContext applicationDbContext)
        {
            this.applicationDbContext = applicationDbContext;
        }

        [Route("")]
        [Route("home")]
        [Route("home/index")]
        [HttpGet]
        public IActionResult Index()
        {
            var db = this.applicationDbContext;
            var fisrtTestItem = db.Test.First();
            var allTest = db.Test;
            ViewBag.title = "Home page";
            ViewBag.testData = fisrtTestItem;
            ViewData.Model = fisrtTestItem;

            //Simple logging example
            Log.Information("test log entry");

            return View(allTest);
        }

        [Route("show/{id}")]
        [HttpGet]
        public IActionResult Show(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var db = this.applicationDbContext;
            var testItem = db.Test.Find(id);

            //Log model object
            Log.Information("Model: {@testItem}", testItem);

            return View(testItem);
        }

        [Route("demos")]
        [HttpGet]
        public IActionResult Demos()
        {
            return View();
        }

        [Route("send-mail")]
        [HttpGet]
        public IActionResult SendMail()
        {
            SmtpClient client = new SmtpClient("smtp.mailtrap.io");
            client.Port = 465;
            client.EnableSsl = true;
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential("1bfcab0ddc67e9", "902610e292a8ec");

            MailMessage mailMessage = new MailMessage();
            mailMessage.From = new MailAddress("noreply@myapp.io");
            mailMessage.To.Add("user@myapp.io");
            mailMessage.Body = "Wow, a .NET Core e-mail";
            mailMessage.Subject = "New e-mail";
            client.Send(mailMessage);

            return View();
        }

        [HttpGet]
        [Route("get-items")]
        public List<Test> Get()
        {
            return this.applicationDbContext.Test.ToList();
        }

        [HttpGet]
        [Route("get-items-json")]
        public JsonResult GetJson()
        {
            var testItems = this.applicationDbContext.Test.ToList();

            return Json(testItems);
        }

        [HttpGet]
        [Route("get-items-iar")]
        public IActionResult GetIar()
        {
            var testItems = this.applicationDbContext.Test.ToList();

            if (testItems == null)
            {
                return NotFound();
            }

            return Ok(testItems);
        }

        [HttpGet]
        [Route("get-items-iar2")]
        public IActionResult GetIar2()
        {
            var testItems = this.applicationDbContext.Test.ToList();

            return new OkObjectResult(new
            {
                status = new
                {
                    message = "Successfully listed items!",
                    statusCode = 200
                },
                data = testItems,
            });
        }
    }
}