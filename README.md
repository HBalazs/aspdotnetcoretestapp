# ASP .NET Core 2.0 Test app
A repository tartalma folyamatosan frissülni fog, ahogyan sikerül haladnom.

#### Kitűzött célok:
- ~~MVC CRUD app létrehozása Razor template engine használatával.~~
- ~~Bootstrap, Fontawesome, jQuery stb. használatával egy layout -> partial -> page view rendszer kialakítása Razor-al~~
- API projekt létrehozása az appon belül
- MariaDB-ből ~~adatok listázása (lista és egy adat megjelentése)~~ és adatok írása adatbázisba
- Form validáció megvalósítása
- ~~Email küldés megvalósítása~~
- Excel-ből adatok beolvasása és adatbázisból Excel generálása
- ~~Logolás megoldása (Serilog)~~
- Ajax lista készítése
- Külső API-ból adatok lekérdezése
- PDF generálása HTML/Razor View alapján
- CSV fileból adat olvasása és CSV-be adatok mentése db-ből
- File fel- és letöltés megvalósítása
- Autentikáció megvalósítása
- JWT autentikáció megvalósítása
- Raw SQL parancsok használatának kipróbálása
- Json response formatter készítése
- ...
> A megvalósított célok át vannak húzva!
#### Tudnivalók:
- A projekt felépítése szerint fontos könyvtárak a **Controllers**, **Models** és **Views**
- A **Views** könyvtárban található egy **Shared** könyvtár ami a **layoutokat** és **partial view**-kat tartalmazza
- A **wwwroot** könyvtár tartalmatzza a **statikus fileokat** (Laravel esetében ez a public könyvtár)
- **logs** könyvtárba kerülnek a **log fileok**
- **Startup.cs** és a **Program.cs** fileok tartalmazzák a fontos alap inicializálásokat/configurációkat pl. logger beállítások
- **appsettings.json** file ugyan a repositoryban nincs fent, de az tartalmazza az **applikáció környezeti beállításait** (Laravelben a .env filenak felel meg)